@extends('layouts.app')

@push('css')
  <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
  <section class="content">
      <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title">Topic</h3>
              <div class="box-tools pull-right">
                  <a href="{{ route('createTopic') }}" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-plus"></i> Add New Data</a>
              </div>
          </div>
          <div class="box-body">
            <table id="table" class="table table-hover table-condensed">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Slug</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
      </div>
  </section>
@endsection

@push('scripts')
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
  <script>
      $(function() {
          $('#table').DataTable({
              processing: true,
              serverSide: true,
              destroy: true,
              order: [ [2, 'desc'] ],
              ajax: {
                  url: '{{ route('dataTopic') }}',
                  type: 'POST',
                  headers: {
                      'X-CSRF-TOKEN': '{{ csrf_token() }}'
                  }
              },
              columns: [
                  { data: 'title', name: 'title' },
                  { data: 'slug', name: 'slug' },
                  { data: 'id', name: 'id' }
              ],
              createdRow: function ( row, data, index ) {
                  /* Action */
                  $("td", row).eq(2).html(
                      '<a href="{{ url('editTopic') }}/'+ data.id +'" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-edit"></i> Edit</a>' +
                      '<form action="{{ url('deleteTopic') }}/'+ data.id +'" method="post" style="display: contents;"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token" value="{{ csrf_token() }}"><button type="submit" class="btn btn-danger btn-flat btn-sm" onclick="return confirm(\'Are you sure to delete this data?\');"><i class="fa fa-trash"></i> Delete</button></form>'
                  );
              }
          });
      });
  </script>
@endpush