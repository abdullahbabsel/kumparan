<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Yajra\DataTables\DataTables;

class NewsController extends Controller
{
    public function index()
    {
        $getTopic = Curl::to('http://localhost/kump_api/api/topic')
                ->asJson()
                ->get();

        return view('pages.news.index', ['allTopic' => $getTopic]);
    }

    public function getDataNews(Request $request) {

        $conditions = [];

        if(!empty($request->title))
            $conditions['title'] = $request->title;
        if(!empty($request->publish))
            $conditions['publish'] = $request->publish;
        if(!empty($request->topic_id))
            $conditions['topic_id'] = $request->topic_id;

        $response = Curl::to('http://localhost/kump_api/api/news')
                ->withData( $conditions )
                ->asJson()
                ->get();

        return Datatables::of($response)->make(true);
    }

    public function create()
    {
        $getTopic = Curl::to('http://localhost/kump_api/api/topic')
                ->asJson()
                ->get();

        return view('pages.news.create', ['allTopic' => $getTopic]);
    }

    public function store(Request $request)
    {
        /* variable */
        unset($request['_token']);
        $request['slug'] = str_slug($request['title'], '-');
        $request['topic_id_tmp'] = serialize($request['topic_id']);       
        ($request['publish'] == 'on')? $request['publish']=2 : $request['publish']=1;

        /* insert to tbl_news */
        $response = Curl::to('http://localhost/kump_api/api/news')
            ->withData( $request->all() )
            ->post();
        $response = json_decode($response);

        /* insert to tbl_news_topic */
        $temp_news_topic = [];
        if($response->topic_id_tmp)
        {
            foreach (unserialize($response->topic_id_tmp) as $key => $value) {
                $temp_news_topic[] = ['news_id' => $response->id, 'topic_id' => $value];
            }       
            
            $response = Curl::to('http://localhost/kump_api/api/news_topic')
                ->withData( $temp_news_topic )
                ->post();
        }

        return redirect('getNews')->with('success', 'Successfully Insert Data');
    }

    public function edit($id)
    {
        $getTopic = Curl::to('http://localhost/kump_api/api/topic')
                ->asJson()
                ->get();

        $data = Curl::to('http://localhost/kump_api/api/news/'.$id)
                ->asJson()
                ->get();

        $topicID = unserialize($data->topic_id_tmp);

        return view('pages.news.edit', ['data' => $data, 'allTopic' => $getTopic, 'topicID' => $topicID]);
    }

    public function update($id, Request $request)
    {
        /* variable */
        unset($request['_token']);
        $request['slug'] = str_slug($request['title'], '-');
        $request['topic_id_tmp'] = serialize($request['topic_id']);
        ($request['publish'] == 'on')? $request['publish']=2 : $request['publish']=1;

        /* update to tbl_news */
        $response = Curl::to('http://localhost/kump_api/api/news/'.$id)
                ->withData( $request->all() )
                ->put();
        $response = json_decode($response);

        /* update to tbl_news_topic */
        $temp_news_topic = [];
        if($response->topic_id_tmp)
        {
            /* deleteAll data by news_id */
            $deleteData = Curl::to('http://localhost/kump_api/api/news_topic/'.$response->id)
                ->asJson()
                ->delete();

            /* insert new data by news_id */
            foreach (unserialize($response->topic_id_tmp) as $key => $value) {
                $temp_news_topic[] = ['news_id' => $response->id, 'topic_id' => $value];
            }       
            
            $response = Curl::to('http://localhost/kump_api/api/news_topic')
                ->withData( $temp_news_topic )
                ->post();
        }

        return redirect('getNews')->with('success', 'Successfully Update Data');
    }

    public function destroy($id)
    {
        $response = Curl::to('http://localhost/kump_api/api/news/'.$id)
                ->withData( array( 'publish' => 3 ) )
                ->put();

        $response = Curl::to('http://localhost/kump_api/api/news/'.$id)
                ->asJson()
                ->delete();

        return redirect('getNews')->with('success', 'Successfully Delete Data');
    }
}
