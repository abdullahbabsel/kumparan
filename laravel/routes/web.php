<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

/*Route::get('/', 'ListController@index');*/
Route::get('/', 'Auth\LoginController@login');
Route::get('/home', 'HomeController@index')->name('home');

/* News */
Route::get('getNews', 'NewsController@index')->name('getNews');
Route::post('dataNews', 'NewsController@getDataNews')->name('dataNews');
Route::get('createNews', 'NewsController@create')->name('createNews');
Route::post('postNews', 'NewsController@store')->name('postNews');
Route::get('editNews/{id}', 'NewsController@edit')->name('editNews/{id}');
Route::post('updateNews/{id}', 'NewsController@update')->name('updateNews/{id}');
Route::delete('deleteNews/{id}', 'NewsController@destroy')->name('deleteNews/{id}');
/* Topic */
Route::get('getTopic', 'TopicController@index')->name('getTopic');
Route::post('dataTopic', 'TopicController@getDataTopic')->name('dataTopic');
Route::get('createTopic', 'TopicController@create')->name('createTopic');
Route::post('postTopic', 'TopicController@store')->name('postTopic');
Route::get('editTopic/{id}', 'TopicController@edit')->name('editTopic/{id}');
Route::post('updateTopic/{id}', 'TopicController@update')->name('updateTopic/{id}');
Route::delete('deleteTopic/{id}', 'TopicController@destroy')->name('deleteTopic/{id}');