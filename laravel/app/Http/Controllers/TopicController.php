<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Yajra\DataTables\DataTables;

class TopicController extends Controller
{
    public function index()
    {
        return view('pages.topic.index');
    }

    public function getDataTopic() {
        $response = Curl::to('http://localhost/kump_api/api/topic')
                ->asJson()
                ->get();

        return Datatables::of($response)->make(true);
    }

    public function create()
    {
        return view('pages.topic.create');
    }

    public function store(Request $request)
    {
        unset($request['_token']);

        $request['slug'] = str_slug($request['title'], '-');

        $response = Curl::to('http://localhost/kump_api/api/topic')
                ->withData( $request->all() )
                ->post();

        return redirect('getTopic')->with('success', 'Successfully Insert Data');
    }

    public function edit($id)
    {
        $response = Curl::to('http://localhost/kump_api/api/topic/'.$id)
                ->asJson()
                ->get();

        return view('pages.topic.edit', ['data' => $response]);
    }

    public function update($id, Request $request)
    {
        unset($request['_token']);

        $request['slug'] = str_slug($request['title'], '-');

        $response = Curl::to('http://localhost/kump_api/api/topic/'.$id)
                ->withData( $request->all() )
                ->put();

        return redirect('getTopic')->with('success', 'Successfully Update Data');
    }

    public function destroy($id)
    {
        $response = Curl::to('http://localhost/kump_api/api/topic/'.$id)
                ->asJson()
                ->delete();

        return redirect('getTopic')->with('success', 'Successfully Delete Data');
    }
}
