@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" />
    <style>
		.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
		.toggle.ios .toggle-handle { border-radius: 20px; }
	</style>
@endpush

@section('content')
<section class="content">
	<form role="form" method="POST" action="{{ url('updateTopic').'/'.$data->id }}">
        {{ csrf_field() }}

		<div class="col-md-12 col-xs-12">
			<div class="box box-primary">
		      	<div class="box-body">
		      		<div class="form-group">
	                  	<label for="Title">Title</label>
	                  	<input class="form-control" type="text" name="title" id="title" placeholder="Enter Title" value="{{ $data->title }}">
	                </div>

		      		<div class="form-group">
		      			<div class="col-md-6 col-xs-12">
		                	<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
		      			</div>
		      			<div class="col-md-6 col-xs-12">
		                	<a href="{{ route('getTopic') }}" class="btn btn-warning btn-block btn-flat">Back</a>
		      			</div>				    	
	                </div>
		      	</div>
		  	</div>
		</div>

	</form>
</section>
@endsection


@push('scripts')
    <script src="{{ asset('plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endpush