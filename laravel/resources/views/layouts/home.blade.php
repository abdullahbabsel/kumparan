<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />

  @stack('css')

</head>
<body class="skin-blue layout-top-nav">
    
  <div class="wrapper">
      
    <header class="main-header">               
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <a href="{{ URL::to('/') }}" class="navbar-brand">{{ config('app.name', 'Laravel') }}</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <ul class="nav navbar-nav">
              @auth
                <li class="active"><a href="{{ url('/home') }}">Home</a></li>
              @else
                <li class="active"><a href="{{ url('login') }}">Login</a></li>
              @endauth
            </ul>                         
          </div>
        </div>
      </nav>
    </header>

    <div class="content-wrapper">
      <section class="content-header">
        <h1>Blank page <small>it all starts here</small></h1>
      </section>

      @yield('content')
    </div>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; {{ date('Y') }} <!--<a href="http://almsaeedstudio.com">Kumparan Test</a>-->.</strong> All rights reserved.
    </footer>

  </div>

  <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dist/js/app.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dist/js/demo.js') }}" type="text/javascript"></script>

  @stack('scripts')

</body>
</html>