<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class ListController extends Controller
{
    public function index()
    {
        $response = Curl::to('http://localhost/kump_api/api/news')
                ->asJson()
                ->get();

        return view('welcome');
    }
}
