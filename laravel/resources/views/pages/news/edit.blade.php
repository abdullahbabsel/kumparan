@extends('layouts.app')

@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" />
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <style>
		.toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
		.toggle.ios .toggle-handle { border-radius: 20px; }
	</style>
@endpush

@section('content')
<section class="content">
	<form role="form" method="POST" action="{{ url('updateNews').'/'.$data->id }}">
        {{ csrf_field() }}

		<div class="col-md-9 col-xs-12">
			<div class="box box-primary">
		      	<div class="box-body">
		      		<div class="form-group">
	                  	<label for="Title">Title</label>
	                  	<input class="form-control" type="text" name="title" id="title" placeholder="Enter Title" value="{{ $data->title }}">
	                </div>
		      		<div class="form-group">
	                  	<label for="Title">Description</label>
	                  	<textarea class="form-control" type="text" name="description" id="description" placeholder="Enter Description">{{ $data->description }}</textarea>
	                </div>
		      		<div class="form-group">
	                  	<label for="Title">Topic</label>
	                  	<select class="form-control js-example-basic-multiple" name="topic_id[]" multiple="multiple">
						  	@foreach($allTopic as $k => $v)
						  		<option value="{{ $v->id }}" 
						  			@if($topicID)
						  				@if(in_array($v->id, $topicID)) selected @endif
						  			@endif
					  			>{{ ucwords($v->title) }}</option>
					  		@endforeach
						</select>
	                </div>

		      		<div class="form-group">
		      			<div class="col-md-6 col-xs-12">
		                	<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
		      			</div>
		      			<div class="col-md-6 col-xs-12">
		                	<a href="{{ route('getNews') }}" class="btn btn-warning btn-block btn-flat">Back</a>
		      			</div>				    	
	                </div>
		      	</div>
		  	</div>
		</div>
		<div class="col-md-3 col-xs-12">
			<div class="box box-info">
			    <div class="box-header with-border text-center">
		      		<h3 class="box-title">Status</h3>
			    </div>
				<div class="box-body text-center">
			    	<input type="checkbox" data-toggle="toggle" data-style="ios" data-on="Publish" data-off="Draft" data-onstyle="success" data-offstyle="warning" name="publish" @if($data->publish == 2) checked @endif />
			    </div>
		  	</div>
		</div>

	</form>
</section>
@endsection


@push('scripts')
    <script src="{{ asset('plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
	<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('.js-example-basic-multiple').select2();
		});
	</script>
@endpush