@extends('layouts.app')

@push('css')
  <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
  <style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
    .toggle.ios .toggle-handle { border-radius: 20px; }
  </style>
@endpush

@section('content')
  <section class="content">

      <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Custom Filter</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="box-body">

          <form role="form" id="form-filter" method="POST" action="{{ route('postNews') }}">
              {{ csrf_field() }}

              <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>
              </div>
              <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="Title">Status</label>
                    <select class="form-control" name="publish" id="publish">
                      <option value="">-- Select --</option>
                      <option value="1">Draft</option>
                      <option value="2">Publish</option>
                      <option value="3">Deleted</option>
                    </select>
                </div>
              </div>
              <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="Title">Topic</label>
                    <select class="form-control" name="topic_id" id="topic_id">
                      <option value="">-- Select --</option>
                      @foreach($allTopic as $k => $v)
                        <option value="{{ $v->id }}">{{ ucwords($v->title) }}</option>
                      @endforeach
                    </select>
                </div>
              </div>

          </form>

        </div>
        <div class="box-footer text-center">
          <button type="submit" id="btn-filter" class="btn btn-primary">Search</button>
          <button type="button" id="btn-reset" class="btn btn-warning">Reset</button>
        </div>
      </div>

      <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title">News</h3>
              <div class="box-tools pull-right">
                  <a href="{{ route('createNews') }}" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-plus"></i> Add New Data</a>
              </div>
          </div>
          <div class="box-body">
            <table id="table" class="table table-hover table-condensed">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Slug</th>
                  <th>Topics</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
      </div>
  </section>
@endsection

@push('scripts')
  <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
      $(document).ready(function() {
          table = $('#table').DataTable({
              processing: true,
              serverSide: true,
              destroy: true,
              order: [ [2, 'desc'] ],
              ajax: {
                  url: '{{ route('dataNews') }}',
                  type: "POST",
                  headers: {
                      'X-CSRF-TOKEN': '{{ csrf_token() }}'
                  },
                  data: function ( data ) {
                      data._token = '{{ csrf_token() }}';
                      data.title = $("#title").val();
                      data.publish = $("#publish").val();
                      data.topic_id = $("#topic_id").val();
                      delete data.search.regex;
                  }
              },
              columns: [
                  { data: 'title', name: 'title' },
                  { data: 'slug', name: 'slug' },
                  { data: 'id', name: 'id' },
                  { data: 'publish', name: 'publish' },
                  { data: 'id', name: 'id' }
              ],
              createdRow: function ( row, data, index ) {
                  var btnStatus = 'danger', textStatus = 'Deleted';

                  /* Topics */
                  if(data.news_topic.length > 0) {
                    var topicTmp = [];

                    $.each( data.news_topic, function( key, value ) {
                      topicTmp = topicTmp+'<button class="btn btn-primary btn-flat btn-sm">'+ value.topic.title +'</button>';
                    })

                    $("td", row).eq(2).html(topicTmp);
                  }
                  
                  /* Status */
                  if(data.publish == 1) {
                      btnStatus = 'warning', textStatus = 'Draft';
                  } else if(data.publish == 2) {
                      btnStatus = 'success', textStatus = 'Publish';
                  }
                  $("td", row).eq(3).html(
                      '<button class="btn btn-'+ btnStatus +' btn-flat btn-sm">'+ textStatus +'</button>'
                  );
                  /* Action */
                  $("td", row).eq(4).html(
                      '<a href="{{ url('editNews') }}/'+ data.id +'" class="btn btn-primary btn-flat btn-sm"><i class="fa fa-edit"></i> Edit</a>' +
                      '<form action="{{ url('deleteNews') }}/'+ data.id +'" method="post" style="display: contents;"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token" value="{{ csrf_token() }}"><button type="submit" class="btn btn-danger btn-flat btn-sm" onclick="return confirm(\'Are you sure to delete this data?\');"><i class="fa fa-trash"></i> Delete</button></form>'
                  );
              }
          });

          $("#btn-filter").click(function(event){
              event.preventDefault();
              table.columns.adjust().draw();
          });
          
          $("#btn-reset").click(function(){
              $("#form-filter")[0].reset();
              table.columns.adjust().draw();
          });
      });
  </script>
@endpush